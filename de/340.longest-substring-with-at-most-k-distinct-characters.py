class Solution(object):
    def lengthOfLongestSubstringKDistinct(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: int
        """
        hmap = {}
        count = 0
        start = 0
        maxLen = 0
        for i,c in enumerate(s):
            if c in hmap:
                hmap[c]+=1
            else:
                hmap[c]=1
                count+=1
                if count>k:
                    while start<=i:
                        cur = s[start]
                        hmap[cur]-=1
                        start+=1
                        if hmap[cur]==0:
                            count-=1
                            del hmap[cur]
                            break
            maxLen = max(maxLen, i-start+1)
        return maxLen
