class Solution(object):
    def maxProfitII(self, prices):
        n = len(prices)
        maxPro = 0;
        for i in range(1, n):
            if prices[i] > prices[i-1]:
                maxPro += prices[i] - prices[i-1]
        return maxPro
    
    def maxProfit(self, k, prices):
        """
        :type k: int
        :type prices: List[int]
        :rtype: int
        """
        n = len(prices)
        if k >= n/2:
            return self.maxProfitII(prices)

        dp = [[0]*(n+1) for _ in range(k+1)]
        for i in range(1, k+1):
            localmax = dp[i-1][0]-prices[0]
            for j in range(2, n+1):
                dp[i][j]=max(dp[i][j-1], localmax+prices[j-1])
                localmax = max(localmax, dp[i-1][j-1]-prices[j-1])
        return dp[k][n]
