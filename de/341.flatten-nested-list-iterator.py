class NestedIterator(object):

    def __init__(self, nestedList):
        """
        Initialize your data structure here.
        :type nestedList: List[NestedInteger]
        """
        self.stk = [[nestedList, 0]]

    def next(self):
        """
        :rtype: int
        """
        stk = self.stk
        self.hasNext()
        clist, idx = stk[-1]
        ret = clist[idx].getInteger()
        stk[-1][1]+=1
        return ret

    def hasNext(self):
        """
        :rtype: bool
        """
        stk = self.stk
        while stk:
            clist, idx = stk[-1]
            if idx==len(clist):
                stk.pop()
            else:
                x = clist[idx]
                if x.isInteger():
                    return True
                stk[-1][1]+=1
                stk.append([x.getList(), 0])
        return False
