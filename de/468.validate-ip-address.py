class Solution(object):
    def validIPAddress(self, IP):
        """
        :type IP: str
        :rtype: str
        """
        ipv4, ipv6 = True, True
        start = 0
        segments = 0
        n = len(IP)
        for i in range(n+1):
            if i!=n and IP[i] not in 'abcdefABCDEF0123456789.:':
                return "Neither"
            
            if i==n or IP[i]==':':
                if i!=n: ipv4=False
                if start>=i or i-start>4:
                    ipv6=False
                elif i!=n:
                    segments+=1
                if segments>7:
                    ipv6=False
                if ipv6: start = i+1
    
            if i==n or IP[i]=='.':
                if i!=n: ipv6=False
                if start>=i or not IP[start:i].isdigit() or int(IP[start:i])>255 or (IP[start]=='0' and len(IP[start:i])!=1):
                    ipv4=False
                elif i!=n:
                    segments+=1
                if segments>3:
                    ipv4=False
                if ipv4: start = i+1
                
            if not (ipv4 or ipv6):
                return "Neither"

        if ipv4 and segments!=3:
            ipv4 = False
        if ipv6 and segments!=7:
            ipv6 = False

        if not (ipv4 or ipv6):
            return "Neither"
        return "IPv4" if ipv4 else "IPv6"
