class Solution(object):
   def calculate(self, s):
      """
      :type s: str
      :rtype: int
      """
      s = re.sub(r'\d+', ' \g<0> ', s)
      s = s.split()
      op = {'+':operator.add, '-':operator.sub,
            '*':operator.mul, '/':operator.floordiv}
      
      total,d,i,func = 0,0,0,op['+']
      while i<len(s):
        if s[i] in '+-':
          total = func(total, d)
          func = op[s[i]]
        elif s[i] in '*/':
          d = op[s[i]](d, int(s[i+1]))
          i+=1
        else:
          d = int(s[i])
        i+=1
      return func(total, d)
