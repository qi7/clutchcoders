class Solution(object):
    def longestPalindromeSubseq(self, s):
        """
        :type s: str
        :rtype: int
        """
        if s == s[::-1]:
            return len(s)
        n = len(s)
        dp = [[1]*n for _ in range(n)]
        for l in range(2, n+1):
            for i in range(n-l+1):
                j = i+l-1
                dp[i][j]=max(dp[i][j-1], dp[i+1][j])
                if s[i]==s[j]:
                    dp[i][j]=max(dp[i][j], 2 if l==2 else (dp[i+1][j-1]+2))

        return dp[0][n-1]
