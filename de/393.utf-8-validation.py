class Solution(object):
    def validUtf8(self, data):
        """
        :type data: List[int]
        :rtype: bool
        """
        cur = 0
        n = len(data)
        while cur<n:
            octet = data[cur] & 255
            count = 4
            if octet>>7 == 0: count=0
            if octet>>5 == 0b110: count=1
            if octet>>4 == 0b1110: count=2
            if octet>>3 == 0b11110: count=3
            if count>3: return False
            cur+=1
            while count>0:
                if cur>=n: return False
                octet = data[cur] & 255
                if octet>>6!=0b10:
                    return False
                cur+=1
                count-=1
        return True
        
