class Solution(object):
    def countDigitOne(self, n):
        """
        :type n: int
        :rtype: int
        """
        base = 1
        count = 0
        while n/base>0:
            highbits = n//(base*10)
            lowbits = n%base
            curbit = n%(base*10)//base
            if curbit<1:
                count+=highbits*base
            elif curbit==1:
                count+=highbits*base+lowbits+1
            elif curbit>1:
                count+=(highbits+1)*base
            base*=10
        
        return count
