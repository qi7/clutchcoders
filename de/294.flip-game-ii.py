class Solution(object):
    def canWin(self, s):
        """
        :type s: str
        :rtype: bool
        """
        def helper(s):
            for i in range(1, len(s)):
              if s[i-1:i+1]=='++':
                if not helper(s[:i-1]+'--'+s[i+1:]):
                  return True
            return False

        return helper(s)
