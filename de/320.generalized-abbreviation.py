def generalize(s):
  def helper(s, start, res, solutions):
    n = len(s)
    if start>=n:
      solutions.append(res)
      return
    
    helper(s, start+1, res+s[start], solutions)
    for i in range(start, n):
      helper(s, i+2, res+str(i-start+1)+s[i+1:i+2], solutions)
  
  solutions=[]
  helper(s, 0, "", solutions)
  return solutions
