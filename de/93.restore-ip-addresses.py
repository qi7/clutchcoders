class Solution(object):
    def restoreIpAddresses(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        def helper(ip, start, ret, solutions):
            n = len(ip)
            if start==n and len(ret)==4:
              solutions.append('.'.join(ret))
              return
        
            # optimization: the rest should not be larger or smaller
            if start+(4-len(ret))*1>n or start+(4-len(ret))*3<n:
              return

            for i in range(3):
              cur = ip[start:start+i+1]
              if cur and 0<=int(cur)<=255 and (cur[0]!="0" or len(cur)==1):
                ret.append(cur)
                helper(ip, start+i+1, ret, solutions)
                ret.pop()

        solutions = []
        ret = []
        helper(s, 0, ret, solutions)
        return solutions
