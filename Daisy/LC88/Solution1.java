package LC88;

/**
 * Created by lipingzhang on 4/4/17.
 */
public class Solution1 {
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int l = m + n -1;
        int i = m - 1, j = n - 1;
        while (i >= 0 && j >= 0) {
            if (nums1[i] > nums2[j]) {
                nums1[l] = nums1[i];
                i--;
            }else {
                nums1[l] = nums2[j];
                j--;
            }

            l--;
        }

        while (j >= 0){
            nums1[l--] = nums2[j--];
        }
    }

    public static void main(String[] args){
        int[] n1 = {2, 0};
        int[] n2 = {1};

        merge(n1, 0, n2, 1);
    }

}
