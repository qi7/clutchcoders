package LC324;

import java.util.Arrays;

/**
 * Created by lipingzhang on 4/6/17.
 */
public class Solution2 {
    public void wiggleSort(int[] nums) {
        if (nums == null || nums.length == 0) {
            return;
        }

        Arrays.sort(nums);

        int[] tmp = new int[nums.length];
        int mid = (nums.length - 1 ) / 2;
        int last = nums.length - 1;
        for(int i = 0; i < nums.length; i++){
            // not use tmp[i++]
            tmp[i] = (i & 1) == 0 ? nums[mid--] : nums[last--];
        }

        for (int i = 0; i < nums.length; i++) {
            nums[i] = tmp[i];
        }
    }
}
