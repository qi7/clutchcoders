package LC452;

import java.util.Arrays;

/**
 * Created by lipingzhang on 4/5/17.
 */
public class Solution1 {
    public int findMinArrowShots(int[][] points) {
        if(points == null || points.length == 0 || points[0].length == 0){
            return 0;
        }

        Arrays.sort(points, (x, y) -> (x[0] - y[0]));
        int terminate = points[0][1];
        // initialized as 1
        int ans = 1;

        int[] point = new int[2];
        for(int i = 1; i < points.length; i++){
            point = points[i];
            // <=, no need to increase ans if point[0] == terminate
            if(point[0] <= terminate){
                terminate = Math.min(terminate, point[1]);
            }else{
                ans++;
                terminate = point[1];
            }
        }

        return ans;
    }
}
