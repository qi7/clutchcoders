package LC140;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lipingzhang on 4/6/17.
 */
public class Solution1 {
    public List<String> wordBreak(String s, List<String> wordDict) {
        List<String> ans = new ArrayList<>();
        if (s == null || s.length() == 0 || wordDict == null || wordDict.size() == 0) {
            return ans;
        }
        return helper(s, wordDict, new HashMap<String, List<String>>());
    }

    private List<String> helper(String s, List<String> wordDict, HashMap<String, List<String>> map) {
        if (map.containsKey(s)) {
            return map.get(s);
        }

        List<String> ans = new ArrayList<>();
        if (s.length() == 0) {
            ans.add("");
            return ans;
        }

        for (String word : wordDict) {
            if (s.startsWith(word)) {
                List<String> subList = helper(s.substring(word.length()), wordDict, map);
                for (String sub : subList) {
                    ans.add(word + (sub.length() == 0 ? "" : " ") + sub);
                }
            }
        }
        map.put(s, ans);
        return ans;
    }
}
