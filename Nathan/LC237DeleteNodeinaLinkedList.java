package LinkedList;

/**
 * Created by Nate He on 4/6/2017.
 * tricky part: didn't include tail
 *               no head supplied
 */
public class LC237DeleteNodeinaLinkedList {
    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;

    }
}
