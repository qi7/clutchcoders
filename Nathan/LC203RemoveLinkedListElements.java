package LinkedList;

/**
 * Created by Nate He on 3/30/2017.
 */
public class LC203RemoveLinkedListElements {
    public class ListNode {
        int val;
        ListNode next;
        ListNode(int x){
            val = x;
        }
    }
    public ListNode removeElements(ListNode head, int val) {
        if(head == null){
            return head;
        }
        ListNode fakehead = new ListNode(val + 1);
        fakehead.next = head;
        ListNode curr = fakehead;
        while(curr.next != null && curr.next.next != null ){
            if(curr.next.val != val){
                curr = curr.next;
            }else{
                curr.next = curr.next.next;
            }
        }
        if (curr.next.val == val) {
            curr.next = null;
        }
        return fakehead.next;

    }
}
