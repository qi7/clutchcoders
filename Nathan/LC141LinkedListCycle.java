package LinkedList;

/**
 * Created by Nate He on 4/4/2017.
 * two pointer
 */
public class LC141LinkedListCycle {
    public boolean hasCycle(ListNode head) {
        if( head == null || head.next == null){
            return false;
        }

        ListNode fast = head;
        ListNode slow = head;
        while(fast != null && fast.next != null){
            fast = fast.next.next;
            slow = slow.next;
            if( fast == slow){
                return true;
            }
        }
        return false;
    }
}
