import java.util.List;
import java.util.ArrayList;
public class LC234 {
	public boolean isPalindrome(ListNode head) {
        if (head == null) return true;
        // extract vals
        List<Integer> values =new  ArrayList<Integer>();
        ListNode list = head;
        while(list != null){
            values.add(list.val);
            
            list = list.next;
            
        }
        int len = values.size();
        int com = len/2;
        for (int i = 0; i<com; i++){
            if (!values.get(i).equals(values.get(len-1-i))){
                return false;
            }
        }
        return true;
        
        
        
    }
	public static void main(String[] args){
		LC234 test = new LC234();
		ListNode li1 = new ListNode(-129);
		ListNode li2 = new ListNode(-129);
		li1.next = li2;
		System.out.println(test.isPalindrome(li1));
	}
}
