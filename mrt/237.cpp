class Solution {
public:
    //delete by copying values
    void deleteNode(ListNode* node) {
        if (node->next && node->next->next)
        //2 or more following nodes
        {
            node->val = node->next->val;
            node->next = node->next->next;
        }else if(node->next)
        //1 following node
        {
            node->val = node->next->val;
            node->next = NULL;
        }
    }
};
