// 109. Convert Sorted List to Binary Search Tree
// Given a singly linked list where elements are sorted in ascending order, convert it to a height balanced BST.
import java.util.*;

class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
}

class TreeNode {
     int val;
     TreeNode left;
     TreeNode right;
     TreeNode(int x) { val = x; }
 }
 
public class LC109 {
	private ListNode node;
	private int level = 0;

	public TreeNode sortedListToBST(ListNode head) {
		if(head == null){
			return null;
		}
		
		int size = 0;
		ListNode runner = head;
		node = head;
		
		while(runner != null){
			runner = runner.next;
			size ++;
		}
		
		return inorderHelper(0, size - 1);
	}

	public TreeNode inorderHelper(int start, int end){
		level++;
		System.out.print("level: " + level);
		System.out.println(" start: " + start + " end " + end + " ");
		if(start > end){
			level--;
			return null;
		}
		
		int mid = start + (end - start) / 2;
		System.out.print("level: " + level);
		System.out.println(" mid: " + mid);
		System.out.print("level: " + level);
		System.out.println(" going left");
		TreeNode left = inorderHelper(start, mid - 1);
		System.out.print("level: " + level);
		if(left == null){
			System.out.println(" left node:null");
		}else{
			System.out.println(" left node:"+left.val);
		}
		System.out.print("level: " + level);
		System.out.println(" node: " + node.val);
		TreeNode treenode = new TreeNode(node.val);
		treenode.left = left;
		node = node.next;

		System.out.print("level: " + level);
		System.out.println(" going right");
		TreeNode right = inorderHelper(mid + 1, end);
		treenode.right = right;
		System.out.print("level: " + level);
		if(right == null){
			System.out.println(" right node:null");
		}else{
			System.out.println(" right node:"+right.val);
		}
		
		level--;
		return treenode;
	}

	public static void main(String[] args){
		LC109 l = new LC109();
		ListNode n1 = new ListNode(1);
		ListNode n2 = new ListNode(2);
		ListNode n3 = new ListNode(3);
		ListNode n4 = new ListNode(4);
		ListNode n5 = new ListNode(5);
		ListNode n6 = new ListNode(6);
		ListNode n7 = new ListNode(7);
		n1.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		n5.next = n6;
		n6.next = n7;
		TreeNode t1 = l.sortedListToBST(n1);
	}
}