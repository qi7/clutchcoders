// 100. Same Tree
// Given two binary trees, write a function to check if they are equal or not.
// Two binary trees are considered equal if they are structurally identical and the nodes have the same value.

 public class TreeNode {
     int val;
     TreeNode left;
     TreeNode right;
     TreeNode(int x) { val = x; }
 }

public class LC100{
	public boolean isSameTree(TreeNode p, TreeNode q) { //recursive
        if(p == null && q == null){
            return true;
        }
        if(p == null && q != null){
            return false;
        }
        if(p != null && q == null){
            return false;
        }
        if(p.val == q.val){
            return (isSameTree(p.left, q.left) && isSameTree(p.right, q.right));
        }
        return false;
    }

    public boolean isSameTree(TreeNode p, TreeNode q) {//bfs with queue

        Queue<TreeNode> pQueue = new LinkedList<>();
        Queue<TreeNode> qQueue = new LinkedList<>();
        
        pQueue.add(p);
        qQueue.add(q);
        
        TreeNode pNode, qNode;
        
        while( (!pQueue.isEmpty()) && (!qQueue.isEmpty()) ){
            pNode = pQueue.poll();
            qNode = qQueue.poll();
            
            if(pNode == null && qNode != null){
                return false;
            }
            if(pNode != null && qNode == null){
                return false;
            }
            if(pNode!= null && qNode!=null){
                if(pNode.val != qNode.val){
                    return false;
                }
                pQueue.add(pNode.left);
                pQueue.add(pNode.right);
                qQueue.add(qNode.left);
                qQueue.add(qNode.right);
            }
        }
        return true;
    }

    public boolean isSameTree(TreeNode p, TreeNode q) {//dfs with stack

        Deque<TreeNode> pStack = new LinkedList<>();
        Deque<TreeNode> qStack = new LinkedList<>();
        
        pStack.push(p);
        qStack.push(q);
        
        TreeNode pNode, qNode;
        
        while( (!pStack.isEmpty()) && (!qStack.isEmpty()) ){
            pNode = pStack.pop();
            qNode = qStack.pop();
            
            if(pNode == null && qNode != null){
                return false;
            }
            if(pNode != null && qNode == null){
                return false;
            }
            if(pNode!= null && qNode!=null){
                if(pNode.val != qNode.val){
                    return false;
                }
                pStack.push(pNode.left);
                pStack.push(pNode.right);
                qStack.push(qNode.left);
                qStack.push(qNode.right);
            }
        }
        return true;
    }

}