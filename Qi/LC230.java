// LC230. Kth Smallest Element in a BST

// Given a binary search tree, write a function kthSmallest to find the kth smallest element in it.
// Note: You may assume k is always valid, 1 ? k ? BST's total elements.
// Follow up: What if the BST is modified (insert/delete operations) often and you need to find the kth smallest frequently? How would you optimize the kthSmallest routine?
// Idea:

// Another example of In-Order Traversal.
// Solution1: Recursive In-Order Traversal.
public class LC230{
    private int counter;
    private int ans;
    public int kthSmallest(TreeNode root, int k) {
        counter = k;
        inOrder(root);
        return ans;
    }

    private void inOrder(TreeNode root){

        if(root.left!= null) inOrder(root.left);
        counter--;
        if(counter == 0){
            ans = root.val;
            return;
        }
        if(root.right!= null) inOrder(root.right);
    }
// Solution2: Iterative In-Order Traversal.
    public int kthSmallest(TreeNode root, int k) {
        Deque<TreeNode> stack = new LinkedList<>();
        stack.push(root);
        while(!stack.isEmpty()){
            while(root!=null){
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            if(--k == 0) break;
            root = root.right;
        }
        return root.val;
    }
}