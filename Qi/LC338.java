// LC338 Counting Bits

// Given a non negative integer number num. For every numbers i in the range 0 ≤ i ≤ num calculate the number of 1's in their binary representation and return them as an array.
// Example: For num = 5 you should return [0,1,1,2,1,2].
// Follow up:
// It is very easy to come up with a solution with run time O(n*sizeof(integer)). But can you do it in linear time O(n) /possibly in a single pass? Space complexity should be O(n).
// Idea:

// For DP, we need to be able to identify the sub problem. In this case, for every number we should try to find the number that have the maximum number of bits that is similar to the current number. So for f[i], that number should be f[i >> 1].
// An easy recurrence for this problem is f[i] = f[i / 2] + i % 2.
public class LC338{
    public int[] countBits(int num) {
        int[] f = new int[num + 1];
        for (int i=1; i<=num; i++) f[i] = f[i >> 1] + (i & 1);
        return f;
    }
}