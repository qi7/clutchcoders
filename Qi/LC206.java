// 206. Reverse Linked List

// Reverse a singly linked list.

// A linked list can be reversed either iteratively or recursively. Could you implement both?

// time complexity is O(n), think about the link between the two nodes in this question.

import java.util.*;


class ListNode {
   int val;
   ListNode next;
   ListNode(int x) { val = x; }
}

public class LC206{
	public ListNode reverseList(ListNode head) { //first solution, thinking about curNode and nextNode
		if(head == null){ //if the list is empty, then return null
            return null;
        }
        ListNode preNode = head;
        ListNode curNode = head.next;
        if(curNode == null){ //if the list contains only one element, then return head itself
            return head;
        }
        ListNode nextNode = curNode.next;
        head.next = null;
        curNode.next = preNode;        
        if(nextNode == null){ // if the list contains two element, reverse the link between the two nodes
            return curNode;
        }
        while(nextNode!=null){ // I took care of previous node and curnode, I only need to take care of curNode and next Node until the whole process are done
            ListNode tmp = nextNode.next;
            nextNode.next = curNode;
            curNode = nextNode;
            nextNode = tmp;
        }
        return curNode;
	}

	public ListNode reverseList2(ListNode head) {//second solution, thinking about preNode and curNode
        if(head == null){ //if the list is empty, then return null
            return null;
        }
        ListNode preNode = head;
        ListNode curNode = preNode.next;
        if(curNode == null){//if the list contains only 1 node, then return head itself;
            return preNode;
        }
        preNode.next = null;
        ListNode tmp;
        while(curNode!=null){//take care of the link between curNode and preNode, until the whole process are done
            tmp = curNode.next;
            curNode.next = preNode;
            preNode = curNode;
            curNode = tmp;
        }
        return preNode;
    }

    public ListNode reverseList3(ListNode head) {//third solution, think one step further, just imagine there is a dummy null node before head, then we can take care of preNode and curNode, without the speical case of lengh 1 list.
    	if(head == null){ //if the list is empty, then return null
            return null;
        }
        ListNode preNode = null;
        ListNode curNode = head;
        ListNode tmp;
        while(curNode!=null){
            tmp = curNode.next;
            curNode.next = preNode;
            preNode = curNode;
            curNode = tmp;
        }
        return preNode;
    }

    public ListNode reverseList4(ListNode head){ //recursive version, if I want to reverse link, I have to pass in the preNode and curNode. The reason is the same as I need a tmp to hold the next node, when I rewrite the next pointer.
        if(head == null){ // even thought I could remove this if part, and it's still accepted, logicly this part should be here.
            return null;
        }
        ListNode pre = null;
        return reverseListR(head, pre);
    }
 
    public ListNode reverseListR(ListNode head, ListNode pre) { 
        if(head == null){ // ending condition, curNode is null, return pre
            return pre;
        }
        ListNode tmp = head.next;
        head.next = pre;
        return reverseListR(tmp, head);
    }

}