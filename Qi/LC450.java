// LC450. Delete Node in a BST

// Given a root node reference of a BST and a key, delete the node with the given key in the BST. Return the root node reference (possibly updated) of the BST.
// Basically, the deletion can be divided into two stages:
// Search for a node to remove.
// If the node is found, delete the node.
// Note: Time complexity should be O(height of tree).
// Idea:

// If the node to be deleted has both left child and right child, after deleting the node, the key part is to reset the tree structure so it remains as BST. One way is use right child to replace the node, then attach the left child tree to the smallest node in the right child tree as its left child. Another way is to get the smallest node in right tree, and use that node to replace the deleted node.
// Solution 1 Recursive.
public class LC450 {
public TreeNode deleteNode(TreeNode root, int key) {
    if(root == null){
        return null;
    }
    if(key < root.val){
        root.left = deleteNode(root.left, key);
    }else if(key > root.val){
        root.right = deleteNode(root.right, key);
    }else{
        if(root.left == null){
            return root.right;
        }else if(root.right == null){
            return root.left;
        }

        TreeNode minNode = findMin(root.right);
        root.val = minNode.val;
        root.right = deleteNode(root.right, root.val);
    }
    return root;
}

private TreeNode findMin(TreeNode node){
    while(node.left != null){
        node = node.left;
    }
    return node;
}
// Solution2 Iterative.
    public TreeNode deleteNode(TreeNode root, int key) {
        if (root==null || root.val==key) return deleteRoot(root);
        TreeNode p=root;

        while (true) { // search the node
            if (key>p.val) {
                if (p.right==null || p.right.val==key) {
                    p.right=deleteRoot(p.right);
                    break;
                }
                p=p.right;
            }
            else {
                if (p.left==null || p.left.val==key) {
                    p.left=deleteRoot(p.left);
                    break;
                }
                p=p.left;
            }
        }
        return root;
    }

    private TreeNode deleteRoot(TreeNode root) {
        if (root==null) return null;
        if (root.right==null) return root.left;
        TreeNode x=root.right; // root.right should be the new root
        while (x.left!=null) x=x.left; // find the left-most node
        x.left=root.left;
        return root.right;
    }
}