// LC381. Insert Delete GetRandom O(1) - Duplicates allowed

// Design a data structure that supports all following operations in average O(1) time.
// Note: Duplicate elements are allowed.
// insert(val): Inserts an item val to the collection.
// remove(val): Removes an item val from the collection if present.
// getRandom: Returns a random element from current collection of elements. The probability of each element being returned is linearly related to the number of same value the collection contains.
// Idea

// Same as LC380, except this time duplicates is allowed, we use a set to keep all the indexes of the same value in the data structure. Use swap trick to keep O(1) for remove.
public class RandomizedCollection {
    Map<Integer, Set<Integer>> myMap;
    List<Integer> data;
    /** Initialize your data structure here. */
    public RandomizedCollection() {
        myMap = new HashMap<>();
        data = new ArrayList<>();
    }

    /** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
    public boolean insert(int val) {
        Set<Integer> indexSet = myMap.get(val);
        if (indexSet == null) { // val is not contained in data
            // add val to data
            data.add(val);
            // update myMap
            indexSet = new HashSet<>();
            indexSet.add(data.size() - 1);
            myMap.put(val, indexSet);
            return true;
        } else { // val has already existed in data
            data.add(val);
            indexSet.add(data.size() - 1);
            return false;
        }
    }

    /** Removes a value from the collection. Returns true if the collection contained the specified element. */
    public boolean remove(int val) {
        Set<Integer> indexSet = myMap.get(val);
        if (indexSet == null) {
            return false;
        } else {
            int valIndex = indexSet.iterator().next();
            // Step 1: Remove val from indexSet.
            // First, swap the last element in data with lastIndex
            int valueInData = data.get(data.size() - 1);
            data.set(valIndex, valueInData);
            // Then, remove val from indexSet.
            indexSet.remove(valIndex);
            // Step 2: update info of valueInData in myMap
            Set<Integer> valueInDataSet = myMap.get(valueInData);
            if (valueInDataSet.remove(data.size() - 1)) {
                valueInDataSet.add(valIndex);
            }
            // Step 3: remove the last val from data
            data.remove(data.size() - 1);
            // also, if indexSet is empty now, clean it up.
            if (indexSet.isEmpty()) {
                myMap.remove(val);
            }
            return true;
        }
    }

    /** Get a random element from the collection. */
    public int getRandom() {
        return data.get((int)(Math.random() * data.size()));
    }
}