// LC538. Convert BST to Greater Tree

// Given a Binary Search Tree (BST), convert it to a Greater Tree such that every key of the original BST is changed to the original key plus sum of all keys greater than the original key in BST.
// Example:
// Input: The root of a Binary Search Tree like this:
// 5
// / \
// 2 13
// Output: The root of a Greater Tree like this:
// 18
// / \
// 20 13
// Idea:

// In-order traversal, just do it in the reverse order: right -> root -> left.
public class LC538{
// Solution 1: Recursive
    private int sum;
    private int preVal;
    private TreeNode pre;
    public TreeNode convertBST(TreeNode root) {
        sum = 0;
        preVal = Integer.MAX_VALUE;
        pre = null;
        reverseInOrder(root);
        return root;
    }

    private void reverseInOrder(TreeNode root){
        if(root == null) return;
        reverseInOrder(root.right);
        int tmp = root.val;
        if(pre!=null && preVal > root.val){ // preVal is to deal with the cases that two of the nodes has the same value
            preVal = root.val;
            root.val += sum;
        }
        sum += tmp;
        pre = root;
        reverseInOrder(root.left);
    }
// Solution 2: Iterative
     public TreeNode convertBST(TreeNode root) {
        int sum = 0;
        Deque<TreeNode> stack = new LinkedList<>();
        TreeNode ans = root;
        while(root!=null || !stack.isEmpty()){
            while(root!=null){
                stack.push(root);
                root = root.right;
            }
            root = stack.pop();
            int tmp = root.val;
            root.val += sum;
            sum += tmp;
            root = root.left;
        }
        return ans;
    }
}