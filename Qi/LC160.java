//160. Intersection of Two Linked Lists

// Write a program to find the node at which the intersection of two singly linked lists begins.


// For example, the following two linked lists:

// A:          a1 → a2
//                    ↘
//                      c1 → c2 → c3
//                    ↗            
// B:     b1 → b2 → b3
// begin to intersect at node c1.


// Notes:

// If the two linked lists have no intersection at all, return null.
// The linked lists must retain their original structure after the function returns.
// You may assume there are no cycles anywhere in the entire linked structure.
// Your code should preferably run in O(n) time and use only O(1) memory.



import java.util.*;


 //Definition for singly-linked list.
class ListNode {
      int val;
      ListNode next;
      ListNode(int x) {
         val = x;
          next = null;
      }
}

public class LC160 {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) { // first get the differnce, then let the longer list pointer advance first
        int lengthA = 0;
        int lengthB = 0;
        
        ListNode pointerA = headA;
        ListNode pointerB = headB;
        
        while(pointerA!=null){
            lengthA ++;
            pointerA = pointerA.next;
        }
        
        while(pointerB!=null){
            lengthB ++;
            pointerB = pointerB.next;
        }
        
        if(lengthA == 0 || lengthB == 0){
            return null;
        }
        
        int lengthDiff = Math.abs(lengthA - lengthB); // get the list difference
        
        pointerA = headA;
        pointerB = headB;
        
        if(lengthA > lengthB){ // advance the longer list pointer first
            while(lengthDiff > 0){
                lengthDiff --;
                pointerA = pointerA.next;
            }
        }else{
            while(lengthDiff > 0){
                lengthDiff --;
                pointerB = pointerB.next;
            }
        }
        
        while(pointerA!=null && pointerB!=null){ // if pointerA and B run into each other, that's the intersection
            if(pointerA == pointerB){
                return pointerA;
            }else{
                pointerA = pointerA.next;
                pointerB = pointerB.next;
            }
        }
        
        return null;
    }


    public ListNode getIntersectionNode2(ListNode headA, ListNode headB) { // combine two lists together, so pointer A and B will run the same length
        if(headA == null || headB == null){
            return null;
        }
        
        ListNode pointerA = headA;
        ListNode pointerB = headB;
        
        while(pointerA != null || pointerB!=null){
            if(pointerA == pointerB){
                return pointerA;
            }else{
                if(pointerA == null && pointerB!=null){
                    pointerA = headB;
                    pointerB = pointerB.next;
                }else if(pointerB == null && pointerA!=null){
                    pointerA = pointerA.next;
                    pointerB = headA;
                }else if(pointerA != null && pointerB!=null){
                    pointerA = pointerA.next;
                    pointerB = pointerB.next;
                }
            }
        }
        return null;
    }



	public static void main(String[] args){
		ListNode n = new ListNode(0);
		ListNode headA = n;
		ListNode headB = n;

		LC160 l = new LC160();
		l.getIntersectionNode(headA, headB);
	}
}
