// LC94. Binary Tree Inorder Traversal

// Given a binary tree, return the inorder traversal of its nodes' values.
// For example:
// Given binary tree [1,null,2,3],
// 1
// \
// 2
// /
// 3
// return [1,3,2].
// Note: Recursive solution is trivial, could you do it iteratively?
// Idea:

// Even thought it's a binary tree problem. But it shows the most basic and important approach for BST, in-order traversal.
public class LC94{
    // Solution1, in-order recursive.
    private List<Integer> list = new LinkedList<>();
    public List<Integer> inorderTraversal(TreeNode root) {
        if(root != null) inOrder(root);
        return list;
    }

    private void inOrder(TreeNode root){
        if(root == null){
            return;
        }
        inOrder(root.left);
        list.add(root.val);
        inOrder(root.right);
    }
// Solution2, in-order iterative.
    public List<Integer> inorderTraversal(TreeNode root) {
        Deque<TreeNode> stack = new LinkedList<>();
        List<Integer> list = new LinkedList<>();
        while(root!=null || !stack.isEmpty()){
            while(root!=null){
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            list.add(root.val);
            root = root.right;
        }
        return list;
    }
}