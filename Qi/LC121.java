// 121. Best Time to Buy and Sell Stock

// Say you have an array for which the ith element is the price of a given stock on day i.
// If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock), design an algorithm to find the maximum profit.
// Example 1:
// Input: [7, 1, 5, 3, 6, 4]
// Output: 5
// max. difference = 6-1 = 5 (not 7-1 = 6, as selling price needs to be larger than buying price)
// Example 2:
// Input: [7, 6, 4, 3, 1]
// Output: 0
// In this case, no transaction is done, i.e. max profit = 0.
// Idea:

public class LC121{
// Solution 1: The idea is to find so far min price.
    public int maxProfit(int[] prices) {
         if (prices.length == 0) {
             return 0 ;
         }        
         int max = 0 ;
         int sofarMin = prices[0] ;
         for (int i = 0 ; i < prices.length ; ++i) {
             if (prices[i] > sofarMin) {
                 max = Math.max(max, prices[i] - sofarMin) ;
             } else{
                sofarMin = prices[i];  
             }
         }         
        return  max ;
     }
// Solution 2:
// Another idea to solve this problem is same as "max subarray problem" using Kadane's Algorithm. Refer to this for more details on the algorithm : https://en.wikipedia.org/wiki/Maximum_subarray_problem
// Here, the logic is to calculate the difference (maxCur += prices[i] - prices[i-1]) of the original array, then the problem becomes finding a contiguous subarray giving maximum profit. If the difference falls below 0, reset it to zero.
    public int maxProfit(int[] prices) {
        int maxCur = 0, maxSoFar = 0;
        for(int i = 1; i < prices.length; i++) {
            maxCur = Math.max(0, maxCur += prices[i] - prices[i-1]);
            maxSoFar = Math.max(maxCur, maxSoFar);
        }
        return maxSoFar;
    }
}