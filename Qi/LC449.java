// LC449. Serialize and Deserialize BST

// Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, or transmitted across a network connection link to be reconstructed later in the same or another computer environment.
// Design an algorithm to serialize and deserialize a binary search tree. There is no restriction on how your serialization/deserialization algorithm should work. You just need to ensure that a binary search tree can be serialized to a string and this string can be deserialized to the original tree structure.
// The encoded string should be as compact as possible.
// Note: Do not use class member/global/static variables to store states. Your serialize and deserialize algorithms should be stateless.
// Idea

// Pre-order to serialize and deserialize. Here we use a "boundary" which utilizes the property of BST, so we don't need to store the null pointer in the string. It makes the string more compact compared to LC297. Serialize and Deserialize Binary Tree.

public class LC449{
    private String splitter = ",";

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuilder sb = new StringBuilder();
        buildString(root, sb);
        return sb.toString();
    }

    private void buildString(TreeNode root, StringBuilder sb) {
        if (root == null) return;
        sb.append(root.val).append(splitter);
        buildString(root.left, sb);
        buildString(root.right, sb);
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (data.length() == 0) return null;
        int[] pos = new int[1];//keep only one value, the purpose is to pass-by-address
        pos[0] = 0;
        return buildTree(data.split(splitter), pos, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    private TreeNode buildTree(String[] nodes, int[] pos, int min, int max) {
        if (pos[0] == nodes.length) return null;

        int val = Integer.valueOf(nodes[pos[0]]);
        if (val < min || val > max) return null; // Go back if we are over the boundary
        TreeNode cur = new TreeNode(val);

        pos[0]++; // update current position
        cur.left = buildTree(nodes, pos, min, val);
        cur.right = buildTree(nodes, pos, val, max);
        return cur;
    }
}