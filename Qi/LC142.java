// 142. Linked List Cycle II
// Given a linked list, return the node where the cycle begins. If there is no cycle, return null.

// Note: Do not modify the linked list.

// Follow up:
// Can you solve it without using extra space?

class ListNode {
     int val;
     ListNode next;
     ListNode(int x) {
         val = x;
         next = null;
     }
}

//Idea:
//We have three nodes: node head, node B where the cycle starts, node A where fast and slow pointer run into each other.
//We have three paths: a, head -> B; b, B -> A (one part of the cycle); c, A -> B (the other part of the cycle);
//When slow and fast pointer run into each other, slow pointer goes through a+b, fast pointer goes through a+b+c+b.
//Since fast pointer goes 2 steps, slow pointer goes 1 steps. So we know 2*(a+b) = a+b+c+b. We have a = c.
//a = c means, if we have a pointer from head, another pointer from A, if they run towards each other, if run into each other at Node B, where the cycle starts.

public class LC142{ // time O(n), space O(1)
	public ListNode detectCycle(ListNode head) { // slow and fast pointer approach same as LC141
        if(head == null){
            return null;
        }
        
        ListNode slow = head;
        ListNode fast = head;
        
        while(fast.next!=null && fast.next.next!=null){
            slow = slow.next;
            fast = fast.next.next;
            if(slow == fast){ // when the fast and slow pointer run into each other at some Node A.
                ListNode node1 = head;
                while( node1 != fast ){ //one pointer start from head, the other from the Node A, the Node they meet each other is where the cycle starts
                    node1 = node1.next;
                    fast = fast.next;
                }
                return node1;
            }
        }
        return null;
    }
}