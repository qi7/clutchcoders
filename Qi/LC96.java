// Given n, how many structurally unique BST's (binary search trees) that store values 1...n?

// For example,
// Given n = 3, there are a total of 5 unique BST's.

//    1         3     3      2      1
//     \       /     /      / \      \
//      3     2     1      1   3      2
//     /     /       \                 \
//    2     1         2                 3


public class LC96{
    public int numTrees(int n) {
            if( n == 0 || n==1 ){
                return 1;
            }
            if(n==2){
                return 2;
            }
            int[] table = new int[n+1];//store result of every step in an array so we won't repeat computation
            table[0] = 1;
            table[1] = 1;
            table[2] = 2;
            for(int i = 3; i <= n; i++){
                for(int k = 1; k <= i; k++){
                    table[i]+=table[k-1]*table[i-k];//this is the recursion formula
                }
            }
            return table[n];
        }
}