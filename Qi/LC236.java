// LC236. Lowest Common Ancestor of a Binary Tree

// Given a binary tree, find the lowest common ancestor (LCA) of two given nodes in the tree.
// According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between two nodes v and w as the lowest node in T that has both v and w as descendants (where we allow a node to be a descendant of itself).”
// Idea:

// Recursive solution function is actually a different meaning from the original question. The function actually means this tree contains at least one of the nodes. Since the in the problem setup, the original tree are guaranteed to contain both of the nodes, then contains at least one of the two nodes is the same thing.

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class LC236 {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if(root == null){
            return null;
        }
        if(root == p || root == q){
            return root;
        }
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);
        
        if(left != null && right != null){
            return root;
        }
        
        if(left == null){
            return right;
        }else{
            return left;
        }
        
    }

    // iteratively 
	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
	    Map<TreeNode, TreeNode> parent = new HashMap<>();
	    Queue<TreeNode> queue = new LinkedList<>();
	    parent.put(root, null);
	    queue.add(root);
	    while (!parent.containsKey(p) || !parent.containsKey(q)) {
	        TreeNode node = queue.poll();
	        if (node != null) {
	            parent.put(node.left, node);
	            parent.put(node.right, node);
	            queue.add(node.left);
	            queue.add(node.right);
	        }
	    }
	    Set<TreeNode> set = new HashSet<>();
	    while (p != null) {
	        set.add(p);
	        p = parent.get(p);
	    }
	    while (!set.contains(q)) {
	        q = parent.get(q);
	    }
    	return q;
	}

	//find path for a sinlge node
	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
		Stack<TreeNode> pStack = new Stack<TreeNode>();
		Stack<TreeNode> qStack = new Stack<TreeNode>();
		TreeNode target = null;
		if (findPath(root, p, pStack) && findPath(root, q, qStack)) {
	 		while (!pStack.isEmpty()) {
	 			TreeNode pNode = pStack.pop();
				if (qStack.contains(pNode))
					target = pNode;
			}
		} 
		return target;
	}

	private boolean findPath(TreeNode root, TreeNode node, Stack<TreeNode> stack) {
		if (root == null)
			return false;
		if (root == node) {
			stack.push(root);
			return true;
		} else {
			if (findPath(root.left, node, stack) ||  findPath(root.right, node, stack)) {
			    stack.push(root);
				return true;
			}
		}
		return false;
	}
}


