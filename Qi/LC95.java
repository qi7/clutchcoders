// 95. Unique Binary Search Trees II
// Given an integer n, generate all structurally unique BST's (binary search trees) that store values 1...n.

// For example,
// Given n = 3, your program should return all 5 unique BST's shown below.

//    1         3     3      2      1
//     \       /     /      / \      \
//      3     2     1      1   3      2
//     /     /       \                 \
//    2     1         2                 3


/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class LC95 {
	//burte recursive
	public List<TreeNode> generateTrees(int n) {
	     List<TreeNode> trees = new ArrayList<TreeNode>();
	     if(n == 0){
	         return trees;
	     }
	     return generateTrees(1,n);
	 }

	public List<TreeNode> generateTrees(int start,int end){             
	    List<TreeNode> trees = new ArrayList<TreeNode>();
	    if(start>end){  trees.add(null); return trees;}

	    for(int rootValue=start;rootValue<=end;rootValue++){
	        List<TreeNode> leftSubTrees=generateTrees(start,rootValue-1);
	        List<TreeNode> rightSubTrees=generateTrees(rootValue+1,end);

	        for(TreeNode leftSubTree:leftSubTrees){
	            for(TreeNode rightSubTree:rightSubTrees){
	                TreeNode root=new TreeNode(rootValue);
	                root.left=leftSubTree;
	                root.right=rightSubTree;
	                trees.add(root);
	            }
	        }
	    }
	    return trees;
	}

	//dp
	public List<TreeNode> generateTrees(int n) {
        ArrayList<TreeNode> nodes = new ArrayList<>();
        if (n == 0) return nodes;
        nodes.add(new TreeNode(1));
        for (int i = 2; i <= n; i++){
            ArrayList<TreeNode> newNodes = new ArrayList<>();
            for (TreeNode treeNode: nodes){
                // 1. Add new node on right top of original tree
                TreeNode newRoot = new TreeNode(i);
                newRoot.left = copyTreeNode(treeNode);
                newNodes.add(newRoot);
                TreeNode nextRoot = treeNode;

                // 2. Add new node on all most right edges of the original tree
                while (nextRoot.right != null){
                    TreeNode next = copyTreeNode(treeNode);
                    newNodes.add(next);
                    // find the current right edge in the newly copied tree
                    while (next.val != nextRoot.val){
                        next = next.right;
                    }
                    
                    TreeNode breakNode = new TreeNode(i);
                    breakNode.left = next.right;
                    next.right = breakNode;
                    // next right edge
                    nextRoot = nextRoot.right;
                }

                // 3. Add new node on the most right-bottom of original tree
                nextRoot.right = new TreeNode(i);
            }
            nodes.addAll(newNodes);
        }
        return nodes;
    }

    private TreeNode copyTreeNode(TreeNode treeNode){
        if (treeNode == null) return null;
        TreeNode newNode = new TreeNode(treeNode.val);
        newNode.left = copyTreeNode(treeNode.left);
        newNode.right = copyTreeNode(treeNode.right);
        return newNode;
    }
}