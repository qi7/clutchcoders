// 226. Invert Binary Tree

//      4
//    /   \
//   2     7
//  / \   / \
// 1   3 6   9
// to
//      4
//    /   \
//   7     2
//  / \   / \
// 9   6 3   1
// Trivia:
// This problem was inspired by this original tweet by Max Howell:
// Google: 90% of our engineers use the software you wrote (Homebrew), but you can’t invert a binary tree on a whiteboard so fuck off.



//Definition for a binary tree node.
class TreeNode {
      int val;
     TreeNode left;
     TreeNode right;
     TreeNode(int x) { val = x; }
}

public class LC226{
	public TreeNode invertTree(TreeNode root) {//recursive
        if(root == null){
            return null;
        }
        TreeNode tmp = root.left;
        root.left = invertTree(root.right);
        root.right = invertTree(tmp);
        return root;
    }

    public TreeNode invertTree2(TreeNode root) {//DFS, using stack
        if(root == null){
            return null;
        }
        Deque<TreeNode> stack = new LinkedList<>();
        stack.push(root);
        TreeNode curNode, tmp;
        while(!stack.isEmpty()){
            curNode = stack.pop();
            tmp = curNode.left;
            curNode.left = curNode.right;
            curNode.right = tmp;
            if(curNode.left!=null){
                stack.push(curNode.left);
            }
            if(curNode.right!=null){
                stack.push(curNode.right);
            }
        }
        return root;
    }

    public TreeNode invertTree3(TreeNode root) { //BFS - or so called level order traversal, using queue
        if(root == null){
            return null;
        }
        // Queue<TreeNode> queue = new PriorityQueue<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        TreeNode curNode, tmp;
        while(!queue.isEmpty()){
            curNode = queue.poll();
            tmp = curNode.left;
            curNode.left = curNode.right;
            curNode.right = tmp;
            if(curNode.left!=null){
                queue.add(curNode.left);
            }
            if(curNode.right!=null){
                queue.add(curNode.right);
            }
        }
        return root;
    }
}