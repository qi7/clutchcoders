// LC530. Minimum Absolute Difference in BST

// Given a binary search tree with non-negative values, find the minimum absolute difference between values of any two nodes.
// Example:
// Input:
// 1
// \
// 3
// /
// 2
// Output:
// 1
// Explanation:
// The minimum absolute difference is 1, which is the difference between 2 and 1 (or between 2 and 3).
// Note: There are at least two nodes in this BST.
// Idea:

// Direct use of in-order traversal.
public class LC530{
// Solution1: Recursive
    private int min;
    private TreeNode pre;
    public int getMinimumDifference(TreeNode root) {
        min = Integer.MAX_VALUE;
        pre = null;
        inOrder(root);
        return min;
    }
    private void inOrder(TreeNode root){
        if(root == null) return;
        inOrder(root.left);
        if(pre!=null){
            min = Math.min(min, root.val - pre.val);
        }
        pre = root;
        inOrder(root.right);
    }
// Solution2: Iterative
    public int getMinimumDifference(TreeNode root) {
        int min = Integer.MAX_VALUE;
        TreeNode pre = null;
        Deque<TreeNode> stack = new LinkedList<>();
        while(root!=null || !stack.isEmpty()){
            while(root!=null){
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            if(pre!=null){
                min = Math.min(min, root.val-pre.val);
            }
            pre = root;
            root = root.right;
        }
        return min;
    }
}