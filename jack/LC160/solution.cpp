#include<iostream>
using namespace std;

struct ListNode {
  int val;
  ListNode *next;
  ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
  public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB){
      if(headA ==NULL || headB == NULL)
        return NULL;

      int lenA = 0;
      ListNode *A = headA;
      while(A){
        lenA ++;
        A = A->next;
      }

      int lenB = 0;
      ListNode *B = headB;
      while(B){
        lenB ++;
        B = B->next;
      }

      if(lenA > lenB){
        int delta = lenA - lenB;
        while(delta--) headA = headA->next;
      }
      else{
        int delta = lenB - lenA;
        while(delta--) headB = headB->next;
      }
      
      while(headA && headB){
        if(headA == headB)
          return headA;
        headA = headA->next;
        headB = headB->next;
      }

      return NULL;
    }

};
int main(){
  Solution s;
  ListNode a = ListNode(3);
  ListNode b = ListNode(4);
  ListNode c = ListNode(5);

  a.next = &c;
  b.next = &c;
  ListNode* res = s.getIntersectionNode(&a, &b);
  cout << res->val << endl;

  return 0;
}
