/**
 * Created by ethan on 4/17/17.
 */
public class Node {
    public String name;
    public Node[] children;
    public Node[] adjacent;
    boolean visited = false;
    boolean marked = false;
}
