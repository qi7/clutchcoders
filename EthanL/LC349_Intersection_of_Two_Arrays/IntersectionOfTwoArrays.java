import java.util.HashSet;
import java.util.Set;



/**
 * Created by Ethan on 2017/3/31.
 */
public class IntersectionOfTwoArrays {
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> s1 = new HashSet<>();
        Set<Integer> intersect = new HashSet<>();
        for (int i : nums1) {
            s1.add(i);
        }
        for (int j : nums2) {
            if (s1.contains(j)) {
                intersect.add(j);
            }
        }
        int[] res = new int[intersect.size()];
        int i = 0;
        for (int k : intersect) {
            res[i++] = k;
        }
        return res;
    }
}
