/**
 * Created by ethan on 4/23/17.
 */
public class TwoSum_Sorted {
    public int[] twoSum(int[] numbers, int target) {
        if (numbers == null || numbers.length == 0) return null;
        int i = 0, j = numbers.length - 1;
        while (i < j) {
            int sum = numbers[i] + numbers[j];
            if (sum < target) {
                ++i;
            } else if (sum > target) {
                j--;
            } else {
                return new int[] {i + 1, j + 1};
            }
        }
        return null;
    }
}
