/*
 * 142. Linked List Cycle II. Given a linked list, return the node where the cycle begins. If there is no cycle, return null.
 *  Note: x-(a)-y-(b)-z-(c)-y
 *  when slow and fast first meet, slow walks a + b fast walks a + b + c + b
 *  as fast is two times faster than slow, we can get a = c
 *  so if there are two pointers starting from X and Z, they will meet at point Y
 */
public class LinkedListCycleII {
    public ListNode detectCycle(ListNode head) {
        if (head == null || head.next == null) return null;
        ListNode slow = head;
        ListNode fast = head;
        while(fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) {
                slow = head;
                //If we need to count the cycle length, we can get a counter
                // int counter = 0;
                while(fast != slow) {
                    fast = fast.next;
                    slow = slow.next;
                    //counter here would be great for getting the length of a
                    //counter++
                }
                return fast;
            }
        }
        return null;
    }
}
