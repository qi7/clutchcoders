/*
 * LC 203. Remove all elements from a linked list of integers that have value val.
 */
public class RemoveLinkedListNode {
    public ListNode removeElements(ListNode head, int val) {
        ListNode cur = new ListNode(0);
        cur.next = head;
        head = cur;
        while(head.next != null) {
            if (head.next.val == val) {
                head.next = head.next.next;
            } else {
                head = head.next;
            }
        }
        return cur.next;
    }
}
