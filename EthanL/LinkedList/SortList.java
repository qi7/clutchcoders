/*
 * LC148 Sort List: Sort a linked list in O(n log n)
 * time using constant space complexity.
 * 1. merge sort
 * 2. find the middle point of linked list
 * 3. merge two sorted list into one
 */
public class SortList {
    public ListNode sortList(ListNode head) {
        if (head == null || head.next ==null) return head;

        ListNode middle = getMiddleNode(head);
        ListNode next = middle.next;
        middle.next = null;
        return mergeTwoList(sortList(head), sortList(next));
    }
    // Find the middle node
    public static ListNode getMiddleNode(ListNode head) {
        ListNode slow = head;
        ListNode fast = head;
        while (fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }
    // Merge two linked list
    public static ListNode mergeTwoList(ListNode headA, ListNode headB) {
        ListNode dummyNode = new ListNode(-1);
        ListNode cur = dummyNode;

        while (headA != null && headB != null) {
            if (headA.val <= headB.val) {
                cur.next = headA;
                headA = headA.next;
            } else {
                cur.next = headB;
                headB = headB.next;
            }
            cur = cur.next;
        }
        cur.next = headA == null ? headB : headA;
        return dummyNode.next;
    }
}
