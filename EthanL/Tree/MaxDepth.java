/**
 * Created by ethan on 4/17/17.
 */

/*
* LC 104: Maximum depth of binary tree
* Given a binary tree, find its maximum depth
* The maximum depth is the number of nodes along the longest path form the root node down to the
* farthest leaf
* */
public class MaxDepth {
    public int maxDepth(TreeNode root) {
        if (root == null) return 0;
        return Math.max(maxDepth(root.left),maxDepth(root.right)) + 1;
    }
}
