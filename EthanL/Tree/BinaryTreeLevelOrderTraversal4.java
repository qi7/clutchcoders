import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by ethan on 4/18/17.
 */
public class BinaryTreeLevelOrderTraversal4 {
    // Solution # 4 BFS queue with dummy node
    public List<List<Integer>> levelOrder(TreeNode root) {
        ArrayList result = new ArrayList();
        if (root == null) return result;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);

        while (!queue.isEmpty()) {
            ArrayList<Integer> level = new ArrayList<Integer>();
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.poll();
                level.add(node.val);
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
                result.add(level);
            }
        }
        return result;
    }

}
