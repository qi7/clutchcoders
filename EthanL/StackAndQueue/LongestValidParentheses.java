import java.util.Stack;

/**
 * Created by demon on 2017/4/3.
 */
/*
* 32. Longest Valid Parentheses
* Given a string containing just the characters '(' and ')', find the length of the longest valid (well-formed) parentheses substring.

For "(()", the longest valid parentheses substring is "()", which has length = 2.

Another example is ")()())", where the longest valid parentheses substring is "()()", which has length = 4.

Subscribe to see which companies asked this question.
* */
public class LongestValidParentheses {
    public int longestValidParentheses(String s) {
        if (s == null || s.length() == 0) return 0;
        int max = 0, start = 0;
        char[] characters = s.toCharArray();
        //Instead of pushing char itself into the stack, we push the position
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < characters.length; i++) {
            if (characters[i] == '(')
                stack.push(i);
            else if (characters[i] == ')') {
                if (stack.empty()) {
                    //record the position of the first left parentheses
                    start = i + 1;
                } else {
                    stack.pop();
                    //if the stack becomes empty, that means that was the last valid parentheses
                    if (stack.empty()) {
                        //max = Math.max(i-start+1, max);
                        stack.push(i);
                    } else {
                        //if stack is not empty, then for current i the longest valid parenthesis  length is
                        // i - start.peek()
                        max = Math.max(i-stack.peek(), max);
                    }
                }
            }
        }
        return max;
    }

    // Dynamic Programming
    public int longestValidParenthesesDP(String s) {
        int maxans = 0;
        int dp[] = new int[s.length()];
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == ')') {
                if (s.charAt(i - 1) == '(') {
                    dp[i] = (i >= 2 ? dp[i - 2] : 0) + 2;
                } else if (i - dp[i - 1] > 0 && s.charAt(i - dp[i - 1] - 1) == '(') {
                    dp[i] = dp[i - 1] + ((i - dp[i - 1]) >= 2 ? dp[i - dp[i - 1] - 2] : 0) + 2;
                }
                maxans = Math.max(maxans, dp[i]);
            }
        }
        return maxans;
    }
}
