'''
111. Minimum Depth of Binary Tree

Given a binary tree, find its minimum depth.

The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.

'''

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def minDepth(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0
        queue = collections.deque([(root, 1)])
        while queue:
            point, size = queue.popleft()
            if not point.left and not point.right:
                return size
            if point.left:
                queue.append((point.left, size+1))
            if point.right:
                queue.append((point.right, size+1))