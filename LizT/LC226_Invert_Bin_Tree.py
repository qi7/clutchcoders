'''
226. Invert Binary Tree

Invert a binary tree.

     4
   /   \
  2     7
 / \   / \
1   3 6   9
to
     4
   /   \
  7     2
 / \   / \
9   6 3   1

'''

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
'''
Using recursion
'''
    def invertTree(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        if not root: 
            return None
        if root.left:
            self.invertTree(root.left)
        if root.right:
            self.invertTree(root.right)
        temp = root.left
        root.left = root.right
        root.right = temp
        return root
        
# class Solution(object):
#     def invertTree(self, root):
#         """
#         :type root: TreeNode
#         :rtype: TreeNode
#         """
#         if not root: 
#             return None
#         stack = [root]          ##create a stack/queue to store the root node in order
#         while stack:
#             head = stack.pop(0) ## check the first node in the stack
#             if head.left:
#                 stack.append(head.left) ## add a node to stack for later checking
#             if head.right:
#                 stack.append(head.right)
#             temp = head.left
#             head.left = head.right
#             head.right = temp
#         return root
            
        
        