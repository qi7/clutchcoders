'''
108. Convert Sorted Array to Binary Search Tree

Given an array where elements are sorted in ascending order, convert it to a height balanced BST.
'''

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def sortedArrayToBST(self, nums):
        """
        :type nums: List[int]
        :rtype: TreeNode
        """
        if not nums:
            return None
        length = len(nums)
        midpoint = length/2
        root = TreeNode(nums[midpoint])
        root.left = self.sortedArrayToBST(nums[:midpoint])   ## recursion for building up the left and right branches. 
        root.right = self.sortedArrayToBST(nums[midpoint+1:])
        return root