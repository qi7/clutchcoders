public class Solution {
    public ListNode removeElements(ListNode head, int val) {
        // check base case
        if (head == null) {
            return head;
        }
        head.next = removeElements(head.next, val);
        // check value
        ListNode dummy = head;
        if (dummy.val == val ) {
            return head.next;
        }
        else {
            return head;
        }
        
    }
}