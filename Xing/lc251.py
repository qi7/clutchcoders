class Vector2D(object):
    
    def __init__(self, vec2d):
        """
            Initialize your data structure here.
            :type vec2d: List[List[int]]
            """
        self.flat = self.flatten(vec2d)
    
    def next(self):
        """
            :rtype: int
            """
        temp = self.flat.pop(0)
        return temp
    
    
    def hasNext(self):
        """
            :rtype: bool
            """
        return self.flat
    
    def flatten(self,vec2d):
        result = []
        for x in vec2d:
            for e in x:
                result.append(e)
        return result


# Your Vector2D object will be instantiated and called as such:
# i, v = Vector2D(vec2d), []
# while i.hasNext(): v.append(i.next())
