# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def nodeNum(self,root):
        if root == None:
            return 0
        else:
            return 1 + self.nodeNum(root.left) + self.nodeNum(root.right)

def leftMost(self,root):
    if root == None:
        return None
        else:
            if root.left == None:
                return root.val
            else:
                return self.leftMost(root.left)
                    
                    def rightMost(self,root):
                        if root == None:
return None
    else:
        if root.right == None:
            return root.val
            else:
                return self.rightMost(root.right)

def isBST(self,root):
    if root == None:
        return True
        else:
            if root.left != None:
                leftValid = root.val > root.left.val and root.val > self.rightMost(root.left) and self.isBST(root.left)
            else:
                leftValid = True
            if root.right != None:
                rightValid = root.val < root.right.val and root.val < self.leftMost(root.right) and self.isBST(root.right)
            else:
                rightValid = True
            return leftValid and rightValid
                
                def largestBSTSubtree(self, root):
                    """
                        :type root: TreeNode
                        :rtype: int
                        """
## level traverse
if self.isBST(root):
    return self.nodeNum(root)
        else:
            left = self.largestBSTSubtree(root.left)
            right = self.largestBSTSubtree(root.right)
            return max(left,right)



