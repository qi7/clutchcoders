class RandomizedSet(object):
    
    def __init__(self):
        #Initialize your data structure here.
        self.nums = set()
    
    
    def insert(self, val):
        #Inserts a value to the set. Returns true if the set did not already contain the specified element.
        #:type val: int
        #:rtype: bool
        if val in self.nums:
            return False
        else:
            self.nums = self.nums | {val}
            return True


def remove(self, val):
    #Removes a value from the set. Returns true if the set contained the specified element.
    #:type val: int
    #:rtype: bool
    if val in self.nums:
        self.nums = self.nums - {val}
            return True
        else:
            return False
                
                
                def getRandom(self):
                    #Get a random element from the set.
#:rtype: int
arr = list(self.nums)
    
    l = len(arr)
        
        index = int(l* random.random())
        if len(arr) == 0:
            return None
    else:
        return arr[index]


