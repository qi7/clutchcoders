class Solution(object):
    def climbStairs(self, n):
        """
            :type n: int
            :rtype: int
            """
        if n < 2:
            return 1
        else:
            distinct = [0 for i in range(n+1)]
            distinct[0] = distinct[1] = 1
            distinct[2] = 2
            for i in range(3,n+1):
                distinct[i] = distinct[i-2] + distinct[i-1]
            return distinct[n]
