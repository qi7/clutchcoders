# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def findMin(self,root):
        temp = root
        if temp == None:
            return None
        else:
            while temp.left!= None:
                temp = temp.left
            return temp.val

def deleteNode(self, root, key):
    if root == None:
        return None
        else:
            if root.val > key:
                root.left = self.deleteNode(root.left,key)
            elif root.val < key:
                root.right = self.deleteNode(root.right,key)
            else:
                if root.left == None:
                    temp = root.right
                    root = None
                    return temp
                elif root.right == None:
                    temp = root.left
                    root = None
                    return temp
                else:
                    mmin = self.findMin(root.right)
                    
                    root.val = mmin
                    
                    root.right = self.deleteNode(root.right,mmin)
    
        return root
