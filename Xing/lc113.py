# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    
    def pathSum(self, root, summ):
        """
            :type root: TreeNode
            :type sum: int
            :rtype: List[List[int]]
            """
        #paths = self.path(root)
        #return [x for x in paths if sum(x) == sum]
        if root == None:
            result = []
        elif root.left == None and root.right == None:
            result = [[root.val]]
        else:
            result,stack = [],[(root,[])]
            while stack:
                node, valArr = stack.pop()
                if node.right:
                    stack.append((node.right,valArr +[node.val]))
                if node.left:
                    stack.append((node.left,valArr+[node.val]))
                if not node.left and not node.right:
                    valArr.append(node.val)
                    result.append(valArr)
        return [x for x in result if sum(x) == summ]

