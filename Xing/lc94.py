class Solution(object):
    def inorderTraversal(self, root):
        if root == None:
            return []
        else:
            result = []
            if root.left != None:
                result += self.inorderTraversal(root.left)
            result += [root.val]
            if root.right != None:
                result += self.inorderTraversal(root.right)
            return result
