class Solution(object):
    def longestValidParentheses(self, s):
        stack = []
        mmax = 0
        left = -1
        for i,e in enumerate(s):
            if e == '(':
                stack.append(i)
            if e == ')':
                if stack ==[]:
                    left = i
                else:
                    stack.pop()
                    if stack == []:
                        mmax = max(mmax, i-left)
                    else:
                        mmax = max(mmax, i-stack[-1])
        return mmax
    
    def longestValidParentheses2(self, s):
        """
            :type s: str
            :rtype: int
            """
        if s == '':
            return 0
        else:
            diff = 0
            mmax = 0
            start = 0
            for i,e in enumerate(s):
                if e == '(':
                    diff += 1
                if e == ')':
                    diff -=1
                if diff < 0:
                    diff = 0
                    start = i+1
                else:
                    if diff == 0:
                        current = i - start + 1
                        if mmax < current:
                            mmax = current
            diff = 0
            right_max = 0
            start = 0
            for i, e in enumerate(s[::-1]):
                if e == ')':
                    diff += 1
                if e == '(':
                    diff -=1
                if diff < 0:
                    diff = 0
                    start = i + 1
                else:
                    if diff == 0:
                        current = i - start + 1
                        if right_max < current:
                            right_max = current
            return max(mmax,right_max)


