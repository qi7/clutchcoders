# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def invertTree(self, root):
        """
            :type root: TreeNode
            :rtype: TreeNode
            """
        if root == None:
            return None
        else:
            left_child = self.invertTree(root.left)
            right_child = self.invertTree(root.right)
            root.right = left_child
            root.left = right_child
        return root
