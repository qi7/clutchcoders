# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def inOrder(self,root):
        if root == []:
            return []
        else:
            result = []
            if root.left:
                result += self.inOrder(root.left)
            result += [root.val]
            if root.right:
                result += self.inOrder(root.right)
            return result
    def recoverTree(self, root):
        #:type root: TreeNode
        #:rtype: void Do not return anything, modify root in-place instead.
        
        inorder = self.inOrder(root)
        i = 0
        while i < len(inorder)-1 and inorder[i] < inorder[i+1]:
            i += 1
        j = len(inorder) - 1
        while j > 0 and inorder[j] > inorder[j-1]:
            j-=1
        first = inorder[i]
        second = inorder[j]
        stack = [root]
        while stack:
            node = stack.pop()
            if node.val == first:
                node.val = second
            else:
                if node.val == second:
                    node.val = first
            if node.right != None:
                stack.append(node.right)
            if node.left != None:
                stack.append(node.left)


# 1 2 6 4 5 3 7
