# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def maxInTree(self,root):
        if root == None:
            return None
        elif root.right == None:
            return root.val
        else:
            return self.maxInTree(root.right)
    def minInTree(self,root):
        if root == None:
            return None
        elif root.left == None:
            return root.val
        else:
            return self.minInTree(root.left)

def isValidBST(self, root):
    if root == None:
        return True
        else:
            if root.left != None:
                left_validation = self.maxInTree(root.left) < root.val and self.isValidBST(root.left)
            else:
                left_validation = True
            
            if root.right != None:
                right_validation = self.minInTree(root.right) > root.val and self.isValidBST(root.right)
            else:
                right_validation = True
            return left_validation and right_validation


