class Solution(object):
    def detectCycle(self, head):
        """
            :type head: ListNode
            :rtype: ListNode
            # x + y + k = 2(x + k)
            # y = x + k
            """
        '''
            d = {}
            while head:
            if head.next in d.keys():
            return head
            else:
            d[head] = 1
            head = head.next
            return None
            '''
        slow = head
        fast = head
        while slow and fast and fast.next: ## python while else
            slow = slow.next
            fast = fast.next.next
            if slow == fast:
                while head != slow:
                    head = head.next
                    slow = slow.next
                return head
        return None
