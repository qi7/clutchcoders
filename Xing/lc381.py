class RandomizedCollection(object):
    import random
    def __init__(self):
        self.nums = []
    
    
    def insert(self, val):
        """
        Inserts a value to the collection. Returns true if the collection did not already contain the specified element.
        :type val: int
        :rtype: bool
        """
        if val in self.nums:
            self.nums.append(val)
            return False
        else:
            self.nums.append(val)
            return True


def remove(self, val):
    """
    Removes a value from the collection. Returns true if the collection contained the specified element.
    :type val: int
    :rtype: bool
    """
    if val in self.nums:
        self.nums.remove(val)
        return True
    else:
        return False


def getRandom(self):
    """
    Get a random element from the collection.
    :rtype: int
    """
    rand = random.random()
    return self.nums[int(rand*len(self.nums))]


# Your RandomizedCollection object will be instantiated and called as such:
# obj = RandomizedCollection()
# param_1 = obj.insert(val)
# param_2 = obj.remove(val)
# param_3 = obj.getRandom()
