# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Codec:
    def serialize(self, root):
        """Encodes a tree to a single string.
            
            :type root: TreeNode
            :rtype: str
            """
        if root == None:
            return []
        else:
            result, queue = [], [root]
            while queue:
                node = queue.pop(0)
                if node != 'null':
                    result.append(node.val)
                    if node.left != None:
                        queue.append(node.left)
                    else:
                        queue.append('null')
                    if node.right != None:
                        queue.append(node.right)
                    else:
                        queue.append('null')
                else:
                    result.append('null')
                    continue
            return result

    def deserialize(self, data):
    """
    Decodes your encoded data to tree.
    :type data: str
    :rtype: TreeNode
    """
        if len(data) == 0:
            return None
        else:
            root = TreeNode(data.pop(0))
            queue = [root]
            while queue and data:
                node = queue.pop(0)
                l = data.pop(0)
                r = data.pop(0)
                if l != 'null':
                    node.left = TreeNode(l)
                    queue.append(node.left)
                else:
                    node.left = None
                if r != 'null':
                    node.right = TreeNode(r)
                    queue.append(node.right)
                else:
                    node.right = None
            return root
