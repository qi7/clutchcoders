class Solution(object):
    def isValidSerialization(self, preorder):
        """
            :type preorder: str
            :rtype: bool
            """
        preorder = preorder.split(',')
        stack = []
        while preorder:
            current = preorder.pop()
            if current == '#':
                stack.append('#')
            else:
                if len(stack) >= 2:
                    stack.pop()
                    stack.pop()
                    stack.append('#')
                
                else:
                    return False
        return stack == ['#']





