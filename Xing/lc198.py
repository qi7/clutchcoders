class Solution(object):
    def rob(self, nums):
        """
            :type nums: List[int]
            :rtype: int
            """
        if len(nums) == 0:
            return 0
        elif len(nums) <= 2:
            return max(nums)
        else:
            f = [0 for i in range(len(nums))]
            f[0] = nums[0]
            f[1] = max(nums[0],nums[1])
            for i in range(2,len(nums)):
                f[i]=max(nums[i]+f[i-2],f[i-1])
            return f[len(nums)-1]
