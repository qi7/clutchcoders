# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Codec:
    def serialize(self, root):
        if root == None:
            return []
        else:
            result, queue = [],[root]
            while queue:
                node = queue.pop(0)
                if node != 'null':
                    result.append(node.val)
                    if node.left != None:
                        queue.append(node.left)
                    else:
                        queue.append("null")
                    if node.right != None:
                        queue.append(node.right)
                    else:
                        queue.append('null')
                else:
                    result.append('null')
            return result


def deserialize(self, data):
    if len(data) == 0:
        return None
        else:
            root = TreeNode(data.pop(0))
            queue = [root]
            while data and queue:
                node = queue.pop(0)
                left = data.pop(0)
                right = data.pop(0)
                if left!= 'null':
                    node.left = TreeNode(left)
                    queue.append(node.left)
                else:
                    node.left = None
                if right != 'null':
                    node.right = TreeNode(right)
                    queue.append(node.right)
                else:
                    node.right = None
        return root



# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.deserialize(codec.serialize(root))
