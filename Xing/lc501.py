# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    ## put tree into arr first, and then count the value. This solution uses extra space: arr to keep the numbers, and count to keep the (num, count) tuples.
    ## time complexity o(n), go through twice. counter's complexity is o(1)
    def findMode(self,root):
        if root == None:
            return []
        else:
            def inorder(node):
                arr = [node.val]
                if node.left != None:
                    arr = inorder(node.left) + arr
                if node.right != None:
                    arr += inorder(node.right)
                return arr
            arr = inorder(root)
            count = collections.Counter(arr)
            ordered_count = list(count.most_common())
            result = [ordered_count[0][0]]
            mmax = ordered_count[0][1]
            for e in ordered_count[1:]:
                if e[1] == mmax:
                    result.append(e[0])
                else:
                    break
            return result

## recursive solution
def findMode2(self, root):
    if root == None:
        return []
        else:
            count = collections.Counter()
            def dfs(node):
                if node:
                    count[node.val] += 1
                    dfs(node.left)
                    dfs(node.right)
            dfs(root)
            max_ct = max(count.itervalues())
            return [k for k, v in count.iteritems() if v == max_ct]


