# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def treeSum(self,root):
        if root == None:
            return 0
        else:
            return root.val + self.treeSum(root.left) + self.treeSum(root.right)
    def convertBST(self, root):
        def convertBSTwithCarri(root,carry):
            if root == None:
                return root
            else:
                root.val = root.val + self.treeSum(root.right) + carry
                convertBSTwithCarri(root.right,carry)
                if root.left != None:
                    convertBSTwithCarri(root.left,root.val)
                return root
        convertBSTwithCarri(root,0)
        return root

