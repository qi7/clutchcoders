# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def reverseTree(self,root):
        if root == None:
            return None
        else:
            left_child = self.reverseTree(root.left)
            right_child = self.reverseTree(root.right)
            root.right = left_child
            root.left = right_child
        return root
    
    def dfs(self,root):
        if root == None:
            return None
        else:
            visited, stack = [],[root]
            while list(filter(None,stack)):
                node = stack.pop()
                
                if node == None:
                    visited.append('null')
                else:
                    visited.append(node.val)
                    stack.extend([node.right,node.left])
            return visited
    
    def isSymmetric(self, root):
        """
            :type root: TreeNode
            :rtype: bool
            """
        if root == None:
            return True
        else:
            right_mirror = self.reverseTree(root.right)
            right_m_dfs = self.dfs(right_mirror)
            left_dfs = self.dfs(root.left)
            return right_m_dfs == left_dfs
