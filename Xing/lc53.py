class Solution(object):
    def maxSubArray(self, nums):
        """
            :type nums: List[int]
            :rtype: int
            """
        maxsofar = nums[0]
        maxsum = nums[0]
        for e in nums[1:]:
            maxsum = max(e,maxsum+e)
            if maxsum > maxsofar:
                maxsofar = maxsum
        return maxsofar
