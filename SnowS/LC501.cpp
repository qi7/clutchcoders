/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
    
    vector<int> ans;
    int cur = 0, occ = 0;
    TreeNode* pre = NULL;
    
    void inorder(TreeNode* root) {
        if(!root) return;
        inorder(root->left);
        if(!pre || pre->val != root->val) cur = 1;
        else cur++;
        if(cur >= occ) {
            if(cur > occ) {ans = {}; occ = cur; }
            ans.push_back(root->val);
        }
        pre = root;
        inorder(root->right);
    }
public:
    vector<int> findMode(TreeNode* root) {
        inorder(root);
        return ans;
    }
};
